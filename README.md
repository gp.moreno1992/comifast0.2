# Comifast0.2

Proyecto integrador que abarca sobre contenedores como parte del proceso enseñanza-aprendizaje de la materia "Computación en la nube<br/>
Autor: Moreno Paú<br/>
*Instrucciones de uso*<br/>
-Clonar repostirorio de gitlab: git clone https://gitlab.com/gp.moreno1992/comifast0.2.git
-Ingresar a la carpeta cd comifast0.2
-Crear directorio src donde va todo el código
-Crear archivos Dockerfile y docker-compose con la configuración correspondiente para cada versión
